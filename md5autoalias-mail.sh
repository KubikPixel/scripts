#!/bin/env bash

ALIASES=/etc/aliases

list() {
    awk '/^#autoalias/ {
    print $2"\t"$3
}' "$ALIASES"
}

add() {
    mail=$( printf "$1" | md5sum | cut -d ' ' -f 1 )
    echo -e "#autoalias $1 $mail@MYDOMAIN.TLD\n$mail:\tMYSELF@MYDOMAIN.TLD" >> "$ALIASES"
    newaliases
    echo "Added $1 as $mail to aliases"
}

delete() {
    mail=$( printf "$1" | md5sum | cut -d ' ' -f 1 )
    tgt=$( mktemp )
    grep -vF "$mail" "$ALIASES" > "$tgt"
    cat "$tgt" > "$ALIASES"
    rm "$tgt"
    newaliases
    echo "Removed $1 as $mail from aliases"
}

usage() {
    echo -e "$0 [-l] [-a addr] [-d addr]"
    echo -e "\t-l\tList aliases"
    echo -e "\t-a addr\tAdd addr to aliases"
    echo -e "\t-d addr\tRemove addr from aliases"
}

if [ $EUID -ne 0 ]
then
    echo "Must run as root or through sudo"
    exit 1
fi

while getopts 'hla:d:' OPT
do
    case $OPT in
        'l')    list
                exit 0
                ;;
        'a')    add "$OPTARG"
                exit 0
                ;;
        'd')    delete "$OPTARG"
                exit 0
                ;;
        *)      usage
                exit 0
                ;;
    esac
done
