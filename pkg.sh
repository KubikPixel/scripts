#!/bin/bash
# List and/or install additional pakages

ADDPKGLIST="$HOME/.local/share/scripts/pkglst/pkglst.txt"
AURPKGLIST="$HOME/.local/share/scripts/pkglst/pkglst_aur.txt"

case $1 in
  '-r')
    pacman -Qqetn | awk '{print $1}' > $ADDPKGLIST; pamac list -mq > $AURPKGLIST
    ;;
  '-i')
    pacman -S --needed - < $ADDPKGLIST; pamac build < $AURPKGLIST
    ;;
  '-l')
    cat $ADDPKGLIST; cat $AURPKGLIST
    ;;
  '-h')
    echo -e "List, read, install Manjaro packages:\n\n-r Read packages and write in list-files\n-i Install packages from list-files\n-l List the packages files\n-h Show this help"
  ;;
esac
