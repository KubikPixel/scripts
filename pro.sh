#!/bin/bash
# Switch in the local projects

projects ()
{
  # Projects directory
  PROJECTSDIR="$HOME/Projekte";
  cd $PROJECTSDIR
  PROJECTS=(*/)

  # Jump to the typet project
  if [ -d "$1" ]; then
    PRO=$1
    cd $PROJECTSDIR/$PRO
  # Select project over a menu
  else
    PS3="Select a current project by number: "
    echo -e "There are currently ${#PROJECTS[@]} projects available.\n"
    select PRO in "${PROJECTS[@]}"; do
        cd $PROJECTSDIR/${PRO}
        break
    done
  fi

  # Info output
  PROJECT=$(echo $PRO | tr -d '[=/=]')
  echo "Current project is $(echo $PROJECT | tr '[:lower:]' '[:upper:]') in directory: $(pwd)"
  ls
  task list project:$PROJECT
}

alias pro=projects
