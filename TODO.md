## Backlog


## ToDo

#### Workflow
@ 2020-06-21
> Build my coding workflow in a script
* [ ] Taskwarrior
* [ ] Timewarrior
* [ ] Pomodoro Timer
* [ ] Python

## Doing

#### Backup
@ 2020-06-07 16:00 UTC
> Backup to the cloud
* [ ] rsync or rclone
* [ ] Backup intervalls
* [ ] Cronejob
* [ ] Delete old Backups
* [x] Timshift config
* [x] rsnapshots?

## Wait


## Done

#### PKG Files clean
> Clean the two PKG files, it's a different between repo and aur to install.
#### CryptoTicker
> Ticker on shell for Crypto-Currencys
* [x] Python
* [x] Tmux
