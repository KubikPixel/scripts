#!/bin/bash

# simple run radio show with visuals
unset TMUX
SHOW=$1
tmux new -s RADIO -d
tmux send-keys -t RADIO 'cmus' C-m
tmux split-window -vp 40 -t RADIO
tmux send-keys -t RADIO 'cava' C-m
tmux split-window -hp 60 -t RADIO
tmux send-keys -t RADIO 'alsamixer' C-m
tmux rename-window -t RADIO:0 Audio
tmux select-window -t RADIO:Audio
tmux select-pane -t 0
tmux attach -t RADIO
