#!/bin/bash

# simple run radio show with visuals
unset TMUX
SHOW=$1
tmux new -s WORK -d
tmux send-keys -t WORK 'nvim' C-m
tmux new-window
tmux split-window -vp 40 -t WORK
tmux send-keys -t WORK 'cava' C-m
tmux rename-window -t WORK:0 Code
tmux rename-window -t WORK:1 Files
tmux rename-window -t WORK:2 Project
tmux rename-window -t WORK:3 Read
tmux select-window -t WORK:Code
tmux attach -t WORK
