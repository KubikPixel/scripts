#!/bin/bash
# Lookup for work in the console

if [[ "$1" == +([0-9]) ]]; then
  curl wrk.ist?p=$1
elif [ $1 ]; then
  curl wrk.ist?reward=$1
else
  curl wrk.ist?help
fi
