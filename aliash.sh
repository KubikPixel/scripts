#!/usr/bin/env bash

# Import custom aliases in .zshrc or/and .bashrc
# source ~/.local/share/scripts/aliash.sh

# MY SCRipts directory
MYSCR="$HOME/.local/share/scripts"
# mycustom ALIASes for the SHell
ALIASH="$HOME/.local/share/scripts/aliash.sh"

alias zshconfig="nvim ~/.zshrc"
alias ohmyzsh="nvim ~/.oh-my-zsh"
alias vim="nvim"
alias vi="nvim"
alias mutt="neomutt"
alias sc="sc-im"
alias todo="taskell"
alias colpal="$MYSCR/colpal.sh"
alias wrk="$MYSCR/wrk.sh"
alias pkg=". $MYSCR/pkg.sh"
alias mfetch="$MYSCR/mfetch.sh"
alias fahrenheit="$MYSCR/fahrenheit.sh"
alias workflow="python $MYSCR/workflow.py"
alias config="/usr/bin/git --git-dir=$HOME/.config/dotfiles/ --work-tree=$HOME"
alias scr="\\ls $MYSCR"
alias diff="nvim -d"
alias ls="lsd -l --group-dirs first"
alias tree="lsd --tree"
alias cat="bat -f"
alias less="bat -f"
alias more="bat -f"
alias cal="task calendar"
alias reddit="tuir"
alias red="tuir"
alias cp="cp -i"                          # confirm before overwriting something
alias df='df -h'                          # human-readable sizes
alias du="du -hld 1"                     # Disk usage deep 1 directory
alias free='free -m'                      # show sizes in MB
alias more=less
alias tv="mpv --fs https://iptv-org.github.io/iptv/languages/deu.m3u"
alias radio="$MYSCR/tmux/radio.tmux"
alias btc="curl -s rate.sx"
alias anon="/opt/tor-browser/Browser/start-tor-browser"
alias nemesis="blightmud -t -c nemesis.de:20000"
alias afterearth="blightmud -c ae-mud.com:9002"
alias timer="$MYSCR/timer"
alias pomo="$MYSCR/timer -q -r2 25 5 25 5 25 5 25 15 &"
# Verry Simple (Smal) Page-View Counter
alias vspvc="curl -s https://thunix.net/\~kubikpixel/count.txt | wc -l | figlet -k -f Small\ Slant | boxes -d peek; date"

# Update config files in repo
alias confup="cd $HOME;config add -u;config commit -S -m 'update config(s) $(date '+%Y-%m-%dT%T')'"

# JRNL ignore history
setopt HIST_IGNORE_SPACE
alias jrnl=" jrnl"

# TUIR - a shell Reddit browser
TUIR_EDITOR="nvim"
TUIR_BROWSER="w3m"
# TUIR_URLVIEWER="urlview"

# Projects Management
source $MYSCR/pro.sh

# SET MANPAGER to bat
export MANPAGER="sh -c 'col -bx | bat -l man -p'"

# ex - archive extractor
# usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# imgv - Image viewer
# usage: imgv <file>
imgv ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.jpeg)   vimiv $1;;
      *.jpg)    vimiv $1;;
      *.png)    vimiv $1;;
      *.bmp)    vimiv $1;;
      *.gif)    vimiv $1;;
      *.tiff)   vimiv $1;;
      *.tif)    vimiv $1;;
      *.ico)    vimiv $1;;
      *)        echo "'$1' cannot be view via imgv()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# URL shortener
shorten() {
  curl -sF "shorten=${1}" https://0x0.st | xclip;
  xclip -o;
}

# Weather forcast
HOMETOWN="Seuzach"
# Show locale forcast on the terminal console in use of curl and
# the https://wttr.in services from @igor_chubin
# Script by reddit.com/u/whetu
wtr() {
  # We require 'curl' so check for it
  if ! type curl; then
    printf -- '%s\n' "[ERROR] wtr: This command requires 'curl', please install it."
    return 1
  fi

  # Handle our variables
  # If no arg is given, default to Seuzach ($HOMETOWN)
  local request curlArgs
  curlArgs="-H \"Accept-Language: ${LANG%_*}\" --compressed -m 10"
  case "${1}" in
    (-h|--help)   request="de.wttr.in/:help" ;;
    (-m|--moon)   request="de.wttr.in/moon" ;;
    (-g|--graphs) shift 1; request="v2.wttr.in/${*:-Seuzach}" ;;
    (*)           request="de.wttr.in/${*:-Seuzach}" ;;
  esac

  # If the width is less than 125 cols, automatically switch to narrow mode
  (( ${COLUMNS:-$(tput cols)} < 125 )) && request+='?n'

  # Finally, make the request
  curl "${curlArgs}" "${request}" 2>/dev/null ||
    printf -- '%s\n' "[ERROR] wtr: Could not connect to wttr.in service."
}
