#!/bin/bash
# Simple Bash Menu Script for select a game to play with dialog

HEIGHT=15
WIDTH=40
CHOICE_HEIGHT=7
BACKTITLE="Select a game to play"
TITLE="Play A Game"
MENU="Choose one of your favorite games:"

OPTIONS=(1 "0 A.D. [DE]"
         2 "The Battle for Wesnoth [DE]"
         3 "Unknown Horizons [DE]"
         4 "Beneath a Steel Sky [EN]"
         5 "Lure of the Temptress [DE]"
         6 "Flight of the Amazon Queen [DE]"
         7 "Cave Story [EN]"
         8 "Daimonin [EN]")

CHOICE=$(dialog --clear \
         --backtitle "$BACKTITLE" \
         --title "$TITLE" \
         --menu "$MENU" \
         $HEIGHT $WIDTH $CHOICE_HEIGHT \
         "${OPTIONS[@]}" \
         2>&1 >/dev/tty)

clear

case $CHOICE in
  1)
    0ad
    ;;
  2)
    wesnoth
    ;;
  3)
    unknown-horizons
    ;;
  4)
    bass
    ;;
  5)
    lure-de
    ;;
  6)
    fotaq-de
    ;;
  7)
    doukutsu
    ;;
  8)
    daimonin
    ;;
esac
