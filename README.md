# scripts

> All scripts have no claim to completeness or even the integrity of your
> computer. Check them before you use them.

A small collection of scripts, that I use daily. I bind them with aliases in
`.bashrc` or `.zshrc` file.

- [x] **aliash.sh** - My custom aliases to import in `.bashrc` or `.zshrc`
- [x] **colpal.sh** - Shell color palete
- [x] **playgame.sh** - Simple menu do start my favorite games
- [x] **wtr** - Weater in my town
- [x] **pkg.sh** - List, read, install packages see `pkg -h`
- [x] **pro.sh** - Switch fast between projects
- [x] **mfetch.sh** - Minimal & simple fetch-like utility
- [x] **fahrenheit.sh** - Simple one line °F to °C calculater (by @wuffle)
- [ ] **wrk.sh** - My workflow
- [ ] **backup.sh** - Backup script
- [X] **btc** - Crypto Currency list
- [x] **TODO.md** - taskell To-Do list


## My _Linux_ environment

I use the community edition of [Manjaro](https://manjaro.org/)
Linux with the [i3](https://i3wm.org/) window manager.


### Install scripts

This files are saved in my home directory.

Type the follow lines in your terminal:

```sh
$ git clone https://codeberg.org/KubikPixel/scripts.git
$ mkdir ~/.local/share/scripts
$ mv ./MyDailyScripts/* ~/.local/share/scripts
$ rmdir ./scripts
$ chmod +x ~/.local/share/scripts/*.sh
$ chmod +x ~/.local/share/scripts/*.py
```


### Aliases

Add aliasses to your `.bashrc` or `.zshrc` with the line in there:

```bash
include ~/.local/share/scripts/aliash.sh
```


### Applications

Additional applications are installed on my system for my daily workflow, all out
of the manjaro stable repo and AUR. For detailed prepare instructions to handle
dotfiles see [here...](https://codeberg.org/KubikPixel/dotfiles)

After then install additional packages:

```sh
$ pkg -i
```


### Prepare Applications

#### ZSH

* Install Oh-My-ZSH
* Set `.Xresources`

#### NeoVim

* Install Vim-Plug & CoC

#### Timewarrior

* Set Taskwarrior trigger for time tracking

#### jrnl

* Set default Journal


## To-Do

For the actual To-Do's see the [TODO.md](TODO.md) file. This file is generated
from [Taskell](https://taskell.app/), a CLI Kanban board.


## License

See [LICENSE](LICENSE) file for details.

```txt
Copyright (c) 2020 KubikPixel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
