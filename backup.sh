#!/bin/bash
# Backup script for my cronejob
# Systembackp is config in timeshift

BAKDIR="$HOME/.local/backup"
INCLUDE="$HOME/.local/backup/babinc.txt"
EXCLUDE="$HOME/.local/backup/bakexc.txt"
TRANSFER="webdav://..."

DATE=$(date +"%Y-%m-%dT%T")

case $1 in
  -d)
    tar -cjpf $BACKDIR/day/bak-$DATE.tar.bz2 --source $INCLUDE --exclude $EXCLUDE
    ;;
  -w)
    ;;
  -m)
    ;;
  -q)
    ;;
  -s)
    ;;
  -y)
    ;;
