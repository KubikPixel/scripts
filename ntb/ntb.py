# @author Raoul René Melcer <raoul.rene.melcer@gmail.com>
# @description NTB (Next Task Bridge)
# @dev a lott


from taskw import TaskWarrior
from markdown import Markdown
from json import JSONDecoder, JSONEncoder
from yaml import load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper


def _initTask():
    tw = TaskWarrior()
    return tw.get_task


def _initKanban():
    md = Markdown("TODO.md").convertFile


def main():
    tasks = _initTask()
    kanban = _initKanban()

if __name__ == "__main__":
    main()
